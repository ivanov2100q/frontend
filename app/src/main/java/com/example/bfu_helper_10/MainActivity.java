package com.example.bfu_helper_10;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        RelativeLayout main_layout = (RelativeLayout) findViewById(R.id.main_layout);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.page_1:
                        main_layout.setBackgroundColor(Color.parseColor("#E0BDFB"));
                        break;
                    case R.id.page_2:
                        main_layout.setBackgroundColor(Color.parseColor("#BDC0FB"));
                        break;
                    case R.id.page_3:
                        main_layout.setBackgroundColor(Color.parseColor("#FBE9BD"));
                        break;
                    default:
                        break;
                }
                return true;
            }
        });
    }
}